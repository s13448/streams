package lab5.service;


import lab5.domain.Person;
import lab5.domain.User;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {

        return users.stream().filter(x->x.getPersonDetails().getAddresses().size()>1)
                                .collect(Collectors.toList());

        
    }

    public static Person findOldestPerson(List<User> users) {

        return users.stream()
                .map(x->x.getPersonDetails())
                    .sorted((x,y)->Integer.compare(y.getAge(),x.getAge()))

                    .findFirst().get();
    }

    public static User findUserWithLongestUsername(List<User> users) {

        return users.stream()
                .sorted((x,y) -> Integer.compare(y.getName().length(), x.getName().length()))
                .findFirst().get();
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {

        return users.stream()
                .map(x->x.getPersonDetails().getName()+" "+x.getPersonDetails().getSurname())
                .collect(joining(","));
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {

        return users.stream()
                .filter(x->x.getName().startsWith("A"))
                .map(x->x.getPersonDetails().getRole().getPermissions())
                .flatMap(x->x.stream())
                .map(x->x.getName())
                .sorted((s1, s2) -> s1.compareToIgnoreCase(s2))
                .collect(Collectors.toList());

    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {

        users.stream()
                .filter(x->x.getPersonDetails().getSurname().startsWith("S"))
                .flatMap(x->x.getPersonDetails().getRole().getPermissions().stream())
                .forEach(x->System.out.println(x.getName().toUpperCase()));
    }
//
    //co oznacza "mających tę samą rolę? referencyjnie?"
//    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {

//        Map<Role,User> roleUserMap= users.stream().collect(Collectors.groupingBy(x->x))
//        List<Role> roles = users.stream().map(x->x.getPersonDetails().getRole()).collect(Collectors.toList());
//        Map<Role,User> roleUserMap = new HashMap<>();
//        roles.forEach(x->{
//
//            List<User> usersList = users.stream().filter(x->x.getPersonDetails().getRole().getName().equals(x.getName()))
//        });

//        List<List<User>> lists = new ArrayList<>();
//        List<Role> roles = users.stream().map(x->x.getPersonDetails().getRole()).collect(Collectors.toList());
//        users.forEach(x->lists.add(x.getPersonDetails().getRole()));
//        users.stream().collect(Collectors.groupingBy())
//        Map<Role,List<User>> roleListMap;
//        roles.forEach(x->lists.add(users.stream().map(y->y.getPersonDetails().getRole()).filter(z->z==x)));
//    }
//
//    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
//
//    }

}
