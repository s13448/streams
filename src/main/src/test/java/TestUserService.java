import lab5.Address;
import lab5.Permission;
import lab5.Role;
import lab5.domain.Person;
import lab5.domain.User;
import lab5.service.UserService;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static lab5.service.UserService.findUsersWhoHaveMoreThanOneAddress;
import static org.assertj.core.api.AssertionsForClassTypes.registerCustomDateFormat;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class TestUserService {

    @Test
    public void findUsersWhoHaveMoreThanOneAddressTest1 (){

        Person person1 = new Person();
        person1.setAddresses(getAdresses(1));
        Person person2 = new Person();
        person2.setAddresses(getAdresses(2));
        Person person3 = new Person();
        person3.setAddresses(getAdresses(3));
        Person person4 = new Person();
        person4.setAddresses(getAdresses(0));

        User user1 = new User();
        user1.setPersonDetails(person1);
        User user2 = new User();
        user2.setPersonDetails(person2);
        User user3 = new User();
        user3.setPersonDetails(person3);
        User user4 = new User();
        user4.setPersonDetails(person4);
        List<User> users = Arrays.asList(user1,user2,user3,user4);

        findUsersWhoHaveMoreThanOneAddress(users);
                assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(users))
                        .hasSize(2)
                .doesNotContain(user4)
                .contains(user3);
    }

    @Test
    public void testFindOldestPerson1(){
        Person person1 = new Person();
        person1.setAge(99);
        Person person2 = new Person();
        person2.setAge(88);
        Person person3 = new Person();
        person3.setAge(12);
        Person person4 = new Person();
        person4.setAge(10);

        List<User> users = packPeopleIntoUsers(Arrays.asList(person1,person2,person3,person4));

        assertThat(UserService.findOldestPerson(users).getAge())
                    .isEqualTo(99);
    }

    @Test
    public void testFindUserWithLongestUsername(){
        User user1 = new User();
        user1.setName("a");
        User user2 = new User();
        user2.setName("a");
        User user3 = new User();
        user3.setName("a");
        User user4 = new User();
        user4.setName("a");

        assertThat(UserService.findUserWithLongestUsername(Arrays.asList(user1,user2,user3,user4)))
                .isEqualTo(user1);
    }

    @Test
    public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(){
        Person person1 = new Person();
        person1.setAge(1);
        Person person2 = new Person();
        person2.setAge(2);
        Person person3 = new Person();
        person3.setAge(3);
        Person person4 = new Person();
        person4.setAge(4);

        List<User> users = packPeopleIntoUsers(Arrays.asList(person1,person2,person3,person4));

        users.forEach(x->{
            x.getPersonDetails().setName(x.getPersonDetails().getAge()+"");
            x.getPersonDetails().setSurname(x.getPersonDetails().getAge()+"");
        });

        assertThat(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users))
                .contains("1 1,2 2,3 3,4 4");

    }


    @Test
    public void testGetSortedPermissionsOfUsersWithNameStartingWithA(){
//        Person person1 = new Person();
//        person1.setAge(1);
//        Person person2 = new Person();
//        person2.setAge(2);
//        Person person3 = new Person();
//        person3.setAge(3);
//        Person person4 = new Person();
//        person4.setAge(4);
//
//        List<User> users = packPeopleIntoUsers(Arrays.asList(person1,person2,person3,person4));
//        //users.get(1).getPersonDetails().getRole().
//        givePermissionsAndNamesBasedOnAge(users);
//
//        assertThat(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users))
//                .doesNotContain("1 1,2 2,3 3,4 4");


    }

    public void givePermissionsAndNamesBasedOnAge(List<User> users){

         users.stream().forEach(x->{
            List<Permission> permissions = new ArrayList<>();

            for(int i=0;i<x.getPersonDetails().getAge();i++){
                Permission permission = new Permission();
                permission.setName(getLine(x.getPersonDetails().getAge()));

            }
             Role role = new Role();
             role.setPermissions(permissions);
            x.getPersonDetails().setRole(role);
            x.setName(x.getPersonDetails().getAge()+"");

        });
    }
//
//    private String getLineDesc(int age) {
//        String result = "";
//        for(int i =0;i<amount;i++){
//            result+="A";
//        }
//        return result;
//    }

    private String getLine(int amount){
        String result = "";
        for(int i =0;i<amount;i++){
            result+="A";
        }
        return result;
    }
    public List<Address> getAdresses(int amount){

        List<Address> result = new ArrayList<>();
        for(int i =0;i<amount;i++) {
            Address address = new Address();
            address.setCity("city"+i);
            address.setCountry("country"+i);
            result.add(address);
        }
        return result;
    }

    private List<User> packPeopleIntoUsers(List<Person> people){

        return people.stream()
                .map(x->{
                    User user = new User();
                    user.setPersonDetails(x);
                    return user;
                })
                .collect(Collectors.toList());
    }

}
